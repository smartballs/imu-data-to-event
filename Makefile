ANDROID_GOPATH= ${GOPATH}/src/gitlab.com/smartballs/imu-data-to-event

all: dep build run

clean:
	rm -rf $(ANDROID_GOPATH)
dep:
	go mod download
build:
	GOOS=linux GO111MODULE=on go build gitlab.com/smartballs/imu-data-to-event/cmd
run:
	go run gitlab.com/smartballs/imu-data-to-event/cmd
test:
	go test gitlab.com/smartballs/imu-data-to-event/... -v -coverprofile .coverage.txt
	go tool cover -func .coverage.txt
coverage: test
	go tool cover -html=.coverage.txt