FROM golang:1.13.1 as build

WORKDIR /app/bin

COPY . .

RUN GOOS=linux GO111MODULE=on go build gitlab.com/smartballs/imu-data-to-event/cmd

FROM ubuntu:19.04

COPY --from=build /app/bin/imu-data-to-event /app/bin/imu-data-to-event

ENTRYPOINT ["/app/bin/imu-data-to-event"]
