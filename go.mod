module gitlab.com/smartballs/imu-data-to-event

go 1.13

require (
	github.com/zeromq/goczmq v4.1.0+incompatible
	gitlab.com/smartballs/driver v0.0.3
)
