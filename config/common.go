package config

import (
	"gitlab.com/smartballs/driver/pkg/cfg"
)

var Common = struct {
	ThresholdUp   float64
	ThresholdDown float64
}{
	ThresholdUp:   cfg.Float64("THRESHOLD_UP").ByDefault(9.81),
	ThresholdDown: cfg.Float64("THRESHOLD_DOWN").ByDefault(0),
}
