package main

import (
	imudatatoevent "gitlab.com/smartballs/imu-data-to-event/internal/imu-data-to-event"
)

func main() {
	imudatatoevent.Start()
}
