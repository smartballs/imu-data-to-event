# Smartball driver

## Limitations

If UDP is used. At the moment only ip v4 addresses are supported.

## Environment variables

| Name | Description | Default value |
|------|-------------|---------------|
| SMARTBALL_ADDR | Address of the smartball(s). If driver mode is set to `udp`, it must be `IP_V4:PORT`. It could be a unicast address to drive only 1 smartball or a multicast address. | 239.0.0.50:8000 |
| DRIVER_ADDR | Address of the driver. If driver mode is set to `udp`, it must be `IP_V4:PORT`. It could be a unicast or a multicast address | 239.0.0.51:9000 |
| DRIVER_MODE | Mode of the driver. Available: `udp`. Susceptible to be extended in the futur.| udp|
| MAX_SIZE_SMARTBALL_INPUT_DATA | Maximum size of the data sent by the smartball. In bytes| 16|